
import com.google.gson.Gson;

import java.util.ArrayList;
import static spark.Spark.*;

public class Main {
    public static void main(String[] args){
        port(3000);
        ArrayList<Estudiante> estudiantes = new ArrayList<>();
        estudiantes.add(new Estudiante(20132045,"Julio","Perez","(212)-421-0133"));
        estudiantes.add(new Estudiante(20132042,"Bryan","RD","(212)-421-0133"));
        estudiantes.add(new Estudiante(20132039,"Saraheemz","RD","(212)-421-0133"));
        Gson tr = new Gson();
        staticFiles.location("/");

        get("/",(req, res)->{
            res.redirect("/public/index.html");
            return "";
        }); // Esta ruta sirve para redireccionar la pagina, cuando se pida localhost:3000, esto te redirecciona al listado de estudiantes.

        post("/guardarEstudiante",(req, res)->{
            ArrayList<String> data = (ArrayList<String>) tr.fromJson(req.body(),ArrayList.class);
            estudiantes.add(new Estudiante(Integer.valueOf(data.get(0)),data.get(1),data.get(2),data.get(3)));
            return "";
        }); // Se crea un estudiante (CREATE).

        get("/getEstudiantes",(req, res)-> {
            res.type("applicacion/json");
            return tr.toJson(estudiantes);
        }); // Enviar todos los estudiantes (READ).

        post("/eliminarEstudiante",(req, res)->{
            eliminarPorMatricula(req.body(),estudiantes);
            return "";
        }); // Se borra un estudiante (DELETE).

        post("/editarEstudiante",(req, res)->{
            ArrayList<String> data = (ArrayList<String>) tr.fromJson(req.body(),ArrayList.class);
            editarEstudiante(data,estudiantes);
            return "";
        }); // Es edita un estudiante (UPDATE).
    }

    private static void eliminarPorMatricula(String mat,ArrayList<Estudiante> est){
        int matricula = Integer.valueOf(mat);
        int count = 0;
        for(Estudiante i: est){
            if (i.getMatricula() == matricula){
                est.remove(count);
                break;
            }
            count++;
        }
    }

    private static void editarEstudiante(ArrayList<String> datos, ArrayList<Estudiante> est){
        int count = 0;
        for (Estudiante i: est){
            int mat =  Integer.valueOf(datos.get(0));
            if (i.getMatricula() == mat){
                est.set(count, new Estudiante(mat,datos.get(1),datos.get(2),datos.get(3)));
                break;
            }
            count++;
        }
    }
}
