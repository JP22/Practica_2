
let http = new XMLHttpRequest();
let students = [];
let editStudent = {};
let edicion = false;

$(document).ready(()=>{
    setMask();
    openListado();
    $('#list').on('click',()=>{
        openListado();
    });
    $('#ing').on('click',()=>{
        openIngreso();
    });
});

function setMask(){
    var phones = [{'mask':'(###)-###-####'}];
    $('#telefono').inputmask({
        mask: phones,
        greedy: false,
        definitions: { '#': { validator: "[0-9]", cardinality: 1}} });
}

function openListado() {
    $('#entrada').hide();
    $('#ingreso').hide();
    $('#listado').show();
    $('#entry').removeClass('active');
    $('#list').addClass('active');
    $('#ing').removeClass('active');
    cargarEstudiantes();
}

function openIngreso(){
    $('#entrada').hide();
    $('#ingreso').show();
    $('#listado').hide();
    $('#entry').removeClass('active');
    $('#list').removeClass('active');
    $('#ing').addClass('active');
    paraIngreso();
    clearFields(['#mat','#nombre','#apellido','#telefono']);
    cargarEstudiantes();
}

function bindInformation(id) {
    let str = '';
    for(let i in students){
        str += `<tr id="${i}">
                   <th>${students[i].matricula}</th>
                   <th>${students[i].nombre}</th>
                   <th>${students[i].apellido}</th>
                   <th>${students[i].telefono}</th>
                   <td>
                       <button class="btn btn-warning btn-xs" onclick="editarEstudiante(${i})" data-toggle="tooltip">
                         <i class="glyphicon glyphicon-pencil"></i>
                        </button>
                    </td>
                    <td>
                        <button class="btn btn-danger btn-xs" onclick="eliminarEstudiante(${i})" data-toggle="tooltip">
                            <i class="glyphicon glyphicon-remove"></i>
                        </button>
                    </td></tr>`;
    }
    $(id).html(str);
}

function cargarEstudiantes() {
    http.open('GET', '/getEstudiantes', true);
    http.onload = function (resp) {
        if (resp.target.response){
            students = JSON.parse(resp.target.response);
            bindInformation('#body');
        }
    };
    http.send(null);
}

function llenarCampos(){
    $("#mat").val(editStudent.matricula);
    $("#nombre").val(editStudent.nombre);
    $("#apellido").val(editStudent.apellido);
    $("#telefono").val(editStudent.telefono);
}

function editarEstudiante(index){
    editStudent = students[index];
    openIngreso();
    paraEdicion();
    llenarCampos();
}

function editEstudiante() {
    let validation =  validateInfo(['#mat','#nombre','#apellido','#telefono'],['#divMat','#divNombre','#divApellido','#divTelefono']);
    if (validation.error !== 1) {
        http.open("POST","/editarEstudiante",true);
        http.setRequestHeader("Content-type", "application/json");
        http.send(JSON.stringify(validation.data));
        setTimeout(function () {
            openListado();
            clearFields(['#mat','#nombre','#apellido','#telefono']);
        },10);
    }
}


function eliminarEstudiante(index){
    let result = confirm("Desea eliminar el estudiante?");
    if (result){
        let st = students[index];
        http.open("POST","/eliminarEstudiante",true);
        http.send(st.matricula);
        students.splice(index, 1);
        $('#body').empty();
        bindInformation("#body");
    }
}
function paraIngreso(){
    $("#titl").show();
    $("#titl2").hide();
    $("#registrar").show();
    $("#editar").hide();
    $("#mat").prop('disabled',false);
    edicion = false;
}

function paraEdicion() {
    $("#titl").hide();
    $("#titl2").show();
    $("#registrar").hide();
    $("#editar").show();
    $("#mat").prop('disabled',true);
    edicion = true;
}

function clearFields(list){
    for (let i in list){
        $(list[i]).val("");
    }
}

function checkNumber(mat){
    let number = parseInt(mat);
    return Number.isInteger(number) ? 0 : 1;
}

function checkMatRepetition(mat){
    let err = 0;
    for(let i in students){
        if (students[i].matricula == mat && !edicion){
            err = 1;
            break
        }
    }
    if (err) alert('No pueden haber 2 estudiantes con la misma matricula');
    return err;
}

function validateInfo(list,divList){
    let err = 0;
    let data = [];
    for(let i in list){
        $(divList[i]).removeClass('has-error');
        if (!$(list[i]).val()) err = 1, $(divList[i]).addClass('has-error');
        data.push($(list[i]).val());
        if (list[i]==='#mat'){
            err += checkMatRepetition($(list[i]).val());
            err += checkNumber($(list[i]).val());
            if (err >= 1) $(divList[i]).addClass('has-error');
        }
    }
    return {error: err, data: data};
}

function guardarEstudiante(){
    let validation = validateInfo(['#mat','#nombre','#apellido','#telefono'],['#divMat','#divNombre','#divApellido','#divTelefono']);
    if (validation.error !== 1) {
        http.open("POST","/guardarEstudiante",true);
        http.setRequestHeader("Content-type", "application/json");
        http.send(JSON.stringify(validation.data));
        students.push({matricula: validation.data[0],nombre:validation.data[1],apellido:validation.data[2],telefono:validation.data[3]});
        alert('Estudiante agregado correctamente');
        clearFields(['#mat','#nombre','#apellido','#telefono']);
        openListado();
    }
}